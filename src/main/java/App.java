
import org.apache.velocity.app.VelocityEngine;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;
import java.util.HashMap;
import static spark.Spark.get;
import static spark.Spark.redirect;

public class App {
    public static void main(String[] args) {
        VelocityEngine configuredEngine = new VelocityEngine();
        configuredEngine.setProperty("runtime.references.strict", true);
        configuredEngine.setProperty("resource.loader", "class");
        configuredEngine.setProperty("class.resource.loader.class",
                "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        VelocityTemplateEngine velocityTemplateEngine = new VelocityTemplateEngine(configuredEngine);

        get("/hello", (req, res) -> {
            HashMap<String, Object> model = new HashMap<>();
            model.put("title", "Keamanan Perangkat Lunak");
            model.put("heading", "Praktikum");
            model.put("paragraph", "Lorem ipum . . .");
            String templatePath = "/views/index.vm";
            return velocityTemplateEngine.render(new ModelAndView(model,templatePath));
        });

        get("/overflow", (req, res) -> {
            HashMap<String, Object> model = new HashMap<>();
            String someInt = req.queryParamOrDefault("someInt", "0");
            int i = Integer.parseInt(someInt);
            byte b = (byte) i;
            model.put("someInt", i);
            model.put("someByte", b);
            String templatePath = "/views/overflow.vm";
            return velocityTemplateEngine.render(new ModelAndView(model, templatePath));
        });

        //latihan 4
        get("/division", (req, res) -> {
           HashMap<String, Object> model = new HashMap<>();
           String aString = req.queryParamOrDefault("a","1");
           String bString = req.queryParamOrDefault("b","1");
           int a = Integer.parseInt(aString);
           int b = Integer.parseInt(bString);
           int c = a/b;
           model.put("a", a);
           model.put("b", b);
           model.put("c", c);
           String templatePath = "views/division.vm";
           return velocityTemplateEngine.render(new ModelAndView(model,templatePath));
        });
    }
}
